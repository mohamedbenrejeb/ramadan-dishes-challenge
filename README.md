# Ramadan dishes challenge



## Cooktime Endpoint (/cooktime) :

##### This endpoint is reading two query params, which are:

    - ingredient: The required ingredient in the dish.

    - day: The *Ramadan* day in which the dish will be cooked


##### This endpoint is responding with:

    - The dishes that contain the given ingredient.

    - With each dish, the relative time to *Asr* prayer at which we should start cooking to have it ready 15 minutes before *Maghrib* prayer.


## Suggestions Endpoint (/suggest)  :

##### This endpoint is reading one query param:

    - day: The *Ramadan* day in which the dish will be cooked.


##### The endpoint is responding with one dish with the same format as the previous endpoint.


## Suggestion Algorithm

#### My algorithm is based on 3 variants, which are:

    1- Cooking Day number,

    2- If cooking day number is even or odd (cookingDay % 2 == 0)

    3- Random number between 1..20

#### 1- Cooking Day number:


    - How this works ?: 

        when the cooking day number increases:

            => the percentage of having a dish with lower duration will increase

            => and the percentage of having a dish with higher duration will automatically decrease


    - Hmmm interesting, can I have some examples ?: 

        example: [day1: lowerDurationPercent = 10, day 28: lowerDurationPercent = 90] // lowerDurationPercent: The percentage of having lower duration


    - Why you add this ?:

        I add this because:
        
            => The cook will get excited in the first days of Ramadan and be ready to cook time-consuming dishes.

            => But in the last days of Ramadan, the cook will be tired and has less enthusiasm, so he wants easy dishes that will not take him much time.


#### 2- If cooking day number is even or odd:

    - How this works ?: 

        when the cooking day number is even:

            => the percentage of having a dish with lower duration will increase

            => and the percentage of having a dish with higher duration will automatically decrease
        
        when the cooking day number is odd:

            => the percentage of having a dish with lower duration will decrease

            => and the percentage of having a dish with higher duration will automatically increase

    - Well, I kind of got it but I need some examples:

        example 1: (

            after the step 1 of our algorithm we got, lowerDurationPercent = 10 and we already have that day = 2 // lowerDurationPercent: The percentage of having lower duration

            2 is even so we will increase the percentage of having lower duration (lowerDurationPercent += 20)

        )

        example 2: (

            well this time we go, lowerDurationPercent = 30, day = 9

            9 is odd se we will not increase the percentage of having lower duration and we will keep it as it is

        )

    - Why you add this ?:

        The purpose of this is to help the cook get some rest and prevent cooking two consecutive dishes that will take a lot of time. 


### 3- Random number between 1..20:

    - How this works ?:

        Choosing a radom number between 1..20 and add this number to the percentage of having lower duration

    - Why you add this ?:

        I add this to add some variation so you can have different so you can have different suggestions with the same day.
