const express = require('express')
const cookTimeController = require('./controllers/cookTimeController')
const suggestController = require('./controllers/suggestController')
const app = express()
const port = 3000

app.get('/cooktime', cookTimeController.getDishesByIngredientsWithCookTime)
app.get('/suggest', suggestController.getOneSuggestion)

app.listen(port, () => {
  console.log(`Ramadan app listening on port ${port}`)
})