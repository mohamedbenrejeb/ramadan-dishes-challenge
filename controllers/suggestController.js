const fs = require('fs')
const utils = require('../utils/utils')
const prayerTimes = require('../utils/prayertimes')

const rawdata = fs.readFileSync('dishes.json')

const sortDishesByDurationAsc = function sortDishesByDurationAsc(dishes) {
    return dishes.sort((a, b) => {
        return a.duration - b.duration;
    })
}

// can't be tested because we have a random value, I can split it and figure a way to test it but I'm lazy :D
const calcLowerDurationPercent = function calcLowerDurationPercent(day) {
    let lowerDurationPercent = 0
    const isEven = parseInt(day) % 2 === 0
    
    // Add day * 2 to lower duration percentage
    lowerDurationPercent += day * 2
    // Add 20 to lower duration percentage if selected day is even
    if (isEven) lowerDurationPercent += 20
    // Add random number from 1-20 to lower duration percentage
    const randomInt = Math.floor(Math.random() * 20) + 1
    lowerDurationPercent += randomInt

    return lowerDurationPercent
}

const calcSelectedDishIndex = function calcSelectedDishIndex(dishesLength, lowerDurationPercent) {
    const higherDurationPercent = 100 - lowerDurationPercent
    const oneDishPercentage = 100 / dishesLength

    // Calculate the selected dish index
    const selectedDishIndex = Math.round(higherDurationPercent / oneDishPercentage)
    return selectedDishIndex
}

const getOneSuggestion = async function getOneSuggestion(req, res) {

    const cookingDay = utils.fixDayFormat(req.query.day)

    if (!utils.isValidDay(cookingDay)) {
        res.status(400)
        res.send("Please enter a valid day!")
        return
    }

    /*
        My algorithm will be based on 3 variants (1- Cooking Day number, 2- If cooking day number is even, 3- Random number):
            - Cooking day number: when the cooking day number increases => the percentage of having a dish with lower duration will increase 
                (ex: day1: lowerDurationPercent = 10, day 28: lowerDurationPercent = 90)
                I add this because:
                    * the cook will get enthusiastic in the first days of ramadan and ready to cook dished that takes long durations 
                    * but in the last days of ramadan the cook will be tired and less enthusiasm so he wants easy dishes that won't took him a lot of time

            - If cooking day number is even the percentage of having a dish with lower duration will increase, the purpose from this is to help the cook to have some rest.
                (ex: in day 1: (1 is not even) the percentage of having longer dish duration will be high with this algorithm we will suggest a dish with lower duration is day 2 (2 is even) so the cook won't get tired) 

            - Random number from 1-20 to add some variation

        Read README.md file for a clear explanation of this algorithm
    */

    const dishes = JSON.parse(rawdata)
    // Sort dished depends on duration from lower to higher
    const sortedDished = sortDishesByDurationAsc(dishes)
    const lowerDurationPercent = calcLowerDurationPercent(cookingDay)
    const selectedDishIndex = calcSelectedDishIndex(dishes.length, lowerDurationPercent)
    const selectedDish = sortedDished[selectedDishIndex]

    prayerTimes(cookingDay, (response, error) => {

        if (error) {
            console.log(response)
            res.status(400)
            res.send("something went wrong!")
            return
        }

        const asrTime = utils.toMinutes(response.timings.Asr)
        const maghribTime = utils.toMinutes(response.timings.Maghrib)
        const dishWithCookTime = {
            name: selectedDish.name,
            ingredients: selectedDish.ingredients,
            cooktime: utils.calcCookTime(selectedDish.duration, asrTime, maghribTime)
        }
        res.status(200)
        res.send(dishWithCookTime)

    })
}

module.exports = { sortDishesByDurationAsc, calcLowerDurationPercent, calcSelectedDishIndex, getOneSuggestion }