const fs = require('fs')
const utils = require('../utils/utils')
const prayerTimes = require('../utils/prayertimes')

const rawdata = fs.readFileSync('./dishes.json')
const dishes = JSON.parse(rawdata)

const getDishesByIngredient = function getDishesByIngredient(dishes, ingredient) {
    return dishes.filter(dish => {
        // I convert all ingredients to lower case so we won't have a problem when the user enters ("Carrot" or "carrot" or even "cArRoT" :D)
        const ingredients = dish.ingredients.map(ing => ing.toLowerCase())
        return ingredients.includes(ingredient.toLowerCase())
    })
}

const getDishesWithCookTime = function getDishesWithCookTime(dishes, asrTime, maghribTime) {
    return dishes.map(dish => ({
        name: dish.name,
        ingredients: dish.ingredients,
        cooktime: utils.calcCookTime(dish.duration, asrTime, maghribTime)
    }))
}

const getDishesByIngredientsWithCookTime = async function(req, res) {

    const ingredient = req.query.ingredient
    const cookingDay = utils.fixDayFormat(req.query.day)

    if (!utils.isValidDay(cookingDay)) {
        res.status(400)
        res.send("Please enter a valid day!")
        return
    }

    if (!utils.isValidIngredient(ingredient)) {
        res.status(400)
        res.send("Please enter an ingredient!")
        return
    }

    const filteredDishes = getDishesByIngredient(dishes, ingredient)

    await prayerTimes(cookingDay, (response, error) => {

        if (error) {
            console.log(response)
            res.status(400)
            res.send("something went wrong!")
            return
        }

        const asrTime = utils.toMinutes(response.timings.Asr)
        const maghribTime = utils.toMinutes(response.timings.Maghrib)
        const dishesWithCookTime = getDishesWithCookTime(filteredDishes, asrTime, maghribTime)
        res.status(200)
        res.send(dishesWithCookTime)

    })

}

module.exports = {getDishesByIngredient, getDishesWithCookTime, getDishesByIngredientsWithCookTime}