const toMinutes = (stringTime) => {
    const time = stringTime.slice(0, 5)
    const hours = parseInt(time.slice(0, 2))
    const minutes = parseInt(time.slice(3, 5))
    const totalMinutes = hours * 60 + minutes
    return totalMinutes
}

const calcCookTime = (duration, asrTime, maghribTime) => {
    
    const availableTimeAfterAsr = maghribTime - asrTime - minutesBeforeMaghribForDishToBeReady
    const remainTime = availableTimeAfterAsr - duration

    let message
    let unit = "minutes"
    if (remainTime === 1 || remainTime === -1) unit = "minute"

    if (remainTime > 0) {
        message = Math.abs(remainTime) + " " + unit + " after Asr"
    } else if (remainTime < 0) {
        message = Math.abs(remainTime) + " " + unit + " before Asr"
    } else {
        message = "Exactly with Asr"
    }

    return message
}

const isValidDay = (day) => {
    const intDay = parseInt(day)
    return !(intDay > 30 || intDay < 1)
}

const isValidIngredient = (ingredient) => {
    return ingredient !== undefined && ingredient !== ""
}

const fixDayFormat = (day) => {
    if (day.length === 1) return "0" + day
    return day
}

const minutesBeforeMaghribForDishToBeReady = 15

module.exports = {toMinutes, calcCookTime, isValidDay, isValidIngredient, fixDayFormat, minutesBeforeMaghribForDishToBeReady}