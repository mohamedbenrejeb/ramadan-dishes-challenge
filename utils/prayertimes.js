const axios = require('axios')

const prayerTimes = (cookingDay, res) => {

    const url = "http://api.aladhan.com/v1/hijriCalendarByCity?city=Sousse&country=Tunisia&method=1&month=09&year=1443"

    axios
        .get(url)
        .then(response => {
            // get the prayer time for the particular day that we want
            const prayerTimes = response.data.data.find(
                ({ date }) => parseInt(date.hijri.day) === parseInt(cookingDay)
            )
            res(prayerTimes, false)
        })
        .catch(error => {
            console.log(error)
            res(error.response, true)
        });
}

module.exports = prayerTimes