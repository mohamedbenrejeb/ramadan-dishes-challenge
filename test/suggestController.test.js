const fs = require('fs')
const suggestController = require('../controllers/suggestController')
const rawdata = fs.readFileSync('dishes.json')
const dishes = JSON.parse(rawdata)

test('test sort dishes by duration Asc function', () => {
    const sortedDished = suggestController.sortDishesByDurationAsc(dishes)
    let lastDuration = 0
    sortedDished.forEach(dish => {
        const condition = dish.duration >= lastDuration
        lastDuration = dish.duration
        expect(
            condition
        ).toBe(true)
    })
})

// I just copy the logic and test it
test('test calc lower duration percent function', () => {
    const day = 2
    let lowerDurationPercent = 0
    const isEven = parseInt(day) % 2 === 0
    
    // Add day * 2 to lower duration percentage
    lowerDurationPercent += day * 2
    // Add 20 to lower duration percentage if selected day is even
    if (isEven) lowerDurationPercent += 20
    // Add random number from 1-20 to lower duration percentage
    const randomInt = Math.floor(Math.random() * 20) + 1
    lowerDurationPercent += randomInt
    expect(
        lowerDurationPercent
    ).toBe(4 + 20 + randomInt)
})

test('test calc selected dish index function', () => {
    const dishesLength = 20
    const lowerDurationPercent = 20
    const selectedDishIndex = suggestController.calcSelectedDishIndex(dishesLength, lowerDurationPercent)
    expect(
        selectedDishIndex
    ).toBe(16)
})