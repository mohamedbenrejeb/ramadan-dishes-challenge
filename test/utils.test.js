const utils = require('../utils/utils')

test('test string time to minutes function', () => {
    const hours = "05"
    const minutes = "15"
    expect(
        utils.toMinutes(hours + ":" + minutes)
    ).toBe(parseInt(hours) * 60 + parseInt(minutes))
})

test('test calculate cook time function', () => {
    expect(
        utils.calcCookTime(200, 500, 615)
    ).toBe("100 minutes before Asr")

    expect(
        utils.calcCookTime(50, 500, 615)
    ).toBe("50 minutes after Asr")

    expect(
        utils.calcCookTime(99, 500, 615)
    ).toBe("1 minute after Asr")

    expect(
        utils.calcCookTime(101, 500, 615)
    ).toBe("1 minute before Asr")

    expect(
        utils.calcCookTime(100, 500, 615)
    ).toBe("Exactly with Asr")
})

test('test day validation function', () => {
    expect(
        utils.isValidDay("30")
    ).toBe(true)

    expect(
        utils.isValidDay("1")
    ).toBe(true)

    expect(
        utils.isValidDay("0")
    ).toBe(false)

    expect(
        utils.isValidDay("31")
    ).toBe(false)
})

test('test ingredient validation function', () => {
    expect(
        utils.isValidIngredient("carrot")
    ).toBe(true)

    expect(
        utils.isValidIngredient("")
    ).toBe(false)

    expect(
        utils.isValidIngredient(undefined)
    ).toBe(false)
})

test('test fix day format function', () => {
    expect(
        utils.fixDayFormat("3")
    ).toBe("03")

    expect(
        utils.fixDayFormat("20")
    ).toBe("20")
})