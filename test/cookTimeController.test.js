const fs = require('fs')
const cookTimeController = require('../controllers/cookTimeController')
const rawdata = fs.readFileSync('dishes.json')
const dishes = JSON.parse(rawdata)
const { minutesBeforeMaghribForDishToBeReady } = require('../utils/utils')

test('test get dishes by ingredient function', () => {
    const ingredient = "caRrot"
    const filteredDishes = cookTimeController.getDishesByIngredient(dishes, ingredient)
    filteredDishes.forEach(dish => 
        expect( // change all ingredients to lower case
            dish.ingredients.map(ing => ing.toLowerCase())
        ).toContain(ingredient.toLowerCase())
    )
})

test('test get dishes with cook time function', () => {
    const asrTime = 500
    const maghribTime = 615
    const dishesWithCookTime = cookTimeController.getDishesWithCookTime(dishes, asrTime, maghribTime)
    for (let i; i++; i < dishes.length) {
        const dish = dishes[i]
        const dishWithCookTime = dishesWithCookTime[i]

        const availableTimeAfterAsr = maghribTime - asrTime - minutesBeforeMaghribForDishToBeReady
        const remainTime = availableTimeAfterAsr - dish.duration
        let cooktime
        let unit = "minutes"
        if (remainTime === 1 || remainTime === -1) unit = "minute"
        
        if (remainTime > 0) {
            cooktime = Math.abs(remainTime) + " " + unit + " after Asr"
        } else if (remainTime < 0) {
            cooktime = Math.abs(remainTime) + " " + unit + " before Asr"
        } else {
            cooktime = "Exactly with Asr"
        }

        expect(
            dishWithCookTime.cooktime
        ).toBe(cooktime)
    }
})